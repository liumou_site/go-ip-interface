#!/usr/bin/env bash
source /etc/profile
f=main.go
pid=`lsof -i:22010 | awk '{print $2}'| sed -n 2p`
lsof -i:22010
if [[ $? -eq 0 ]];then
  kill -9 $pid
fi
if [[ -e ./$f ]];then
  go run main.go &
else
  echo "文件不存在: $f"
  exit 2
fi