import os
import shutil
import sys
from shutil import copy2

from ColorInfo import ColorLogger


class GoBuild:
    def __init__(self, file, name=None, windows=False, darwin=False, linux=True, arm64=True, mips64=True, amd64=True):
        """

		:param file: 需要构建的主文件(例如: main.go)
		:param name: 生成的执行文件主名称(例如: install)
		:param windows: 是否打包Windows平台
		:param linux: 是否打包Linux平台
		:param arm64: 是否打包arm64架构
		:param mips64: 是否打包mips64架构
		:param amd64: 是否打包amd64架构
		"""
        self.all = False
        self.darwin = darwin
        self.go = "go"
        self.name = name
        self.arch_list = []
        self.os_list = []
        self.amd64 = amd64
        self.mips64 = mips64
        self.arm64 = arm64
        self.linux = linux
        self.windows = windows
        self.file = file
        self.basename = ""
        self.archs = "X86_64"
        self.os_type = ""
        self.exe = ""
        self.tmp = ""
        self.logger = ColorLogger()
        self.pwd = None
        self.init()

    def init(self):
        if self.all:
            self.logger.info("开启常用平台")
            self.arm64 = True
            self.amd64 = True
            self.mips64 = True
            self.windows = True
            self.darwin = True
            self.linux = True
        if self.arm64:
            self.arch_list.append("arm64")
        if self.mips64:
            self.arch_list.append("mips64le")
        if self.amd64:
            self.arch_list.append("amd64")
        if self.linux:
            self.os_list.append("linux")
        if self.windows:
            self.os_list.append("windows")
        if self.darwin:
            self.os_list.append("darwin")
        if self.name is None:
            self.basename = str(os.path.basename(self.file)).replace(".go", "")
        else:
            self.basename = self.name

    def delete(self):
        """
		开始删除生成的临时文件
		:return:
		"""
        tmp = os.path.join(os.getcwd(), self.tmp)
        try:
            os.remove(path=self.tmp)
            self.logger.debug("删除成功: ", tmp)
        except Exception as e:
            self.logger.error(f"删除出错 - [{tmp} ] : ", str(e))

    def copy(self):
        """
		复制执行文件
		:return:
		"""
        dst = os.path.join("./client", self.exe)
        dst = os.path.join(self.exe)
        dst = os.path.join(os.getcwd(), dst)
        dst = str(dst).replace("amd64", "X86_64")
        self.logger.debug("开始复制: ", dst)
        if os.path.isfile(self.tmp):
            try:
                copy2(src=self.tmp, dst=dst)
                self.delete()
            except Exception as e:
                self.logger.error("复制失败: ", str(e))
        else:
            self.logger.warning("文件不存在: ", self.tmp)

    def build(self):
        if self.archs == "amd64":
            self.archs = "X86_64"
        self.logger.debug("构建系统: ", self.os_type)
        self.logger.debug("构建架构: ", self.archs)
        self.exe = self.basename + "_" + self.os_type + "-" + self.archs
        self.tmp = str(os.path.basename(self.file)).replace(".go", "")
        if self.os_type == "windows":
            self.exe = self.exe + ".exe"
            self.tmp = self.tmp + ".exe"
        c = f"{self.go} build {self.file}"
        if os.system(c) == 0:
            self.logger.info("构建成功,正在生成: ", self.exe)
            self.copy()
        else:
            self.logger.error("构建失败: ", self.exe)
            print(c)
            sys.exit(2)

    def ost(self, o):
        os.environ['GOOS'] = o
        self.os_type = o

    def arch(self, arch):
        if arch == "X86_64":
            arch = "amd64"
        os.environ['GOARCH'] = arch
        self.archs = arch
        self.build()

    def start(self):
        if self.pwd is not None:
            os.chdir(self.pwd)
        if os.path.isdir("./client"):
            shutil.rmtree("./client")
        os.mkdir("./client")
        for i in self.os_list:
            self.ost(i)
            if i == "linux":
                self.logger.debug("架构列表: ", self.arch_list)
                for a in self.arch_list:
                    self.arch(arch=a)
            else:
                self.arch(arch="X86_64")


if __name__ == "__main__":
    up = GoBuild(file="main.go", name="ips", windows=True, darwin=True)
    up.all = True  # 打包所有环境
    # up = GoBuild(file="main.go", name="ips", mips64=False, linux=True, darwin=False, arm64=False, amd64=True, windows=False)
    up.start()
