package model

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"os"
)

type Server struct {
	Port    int         // 端口
	Run     *gin.Engine // 引擎
	Ico     string      // 图标文件路径
	Tmpl    string      // 模板路径
	Bg      string      // 设置背景图片
	Urls4   string      // URL信息
	Urls6   string      // URL信息
	Debug   bool        // 显示更多运行信息
	Pids    bool        // 是否写入Pid文件
	Listen  string      // 监听地址
	Listen4 bool        // 是否监听IPv4
	Listen6 bool        // 是否监听IPv6
}

func (api *Server) Start() {
	api.StaticHtml()
	api.StaticIco()
	api.ApiJson()
	api.Api()
	api.IndexHtml()
	go func() {
		err := api.Run.Run(fmt.Sprintf("[::]:%d", api.Port+1))
		if err != nil {
			os.Exit(2)
		}
	}()

	go func() {
		err := api.Run.Run(fmt.Sprintf("127.0.0.1:%d", api.Port))
		if err != nil {
			os.Exit(2)
		}
	}()
	select {}
}
