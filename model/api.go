package model

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func (api *Server) Api() {
	api.Run.GET("/api", func(c *gin.Context) {
		// 获取客户端IP地址
		// 如果有代理服务器，会返回代理服务器的IP
		clientIP := c.ClientIP()

		// 检查是否有X-Forwarded-For头
		if forwardedFor := c.GetHeader("X-Forwarded-For"); forwardedFor != "" {
			// 如果有，使用X-Forwarded-For头中的IP地址
			clientIP = forwardedFor
		}

		// 返回JSON响应
		c.String(http.StatusOK, clientIP)
	})
}

type Res struct {
	Code int    // 代码
	Msg  string // 信息
	Ip   string // 访客IP地址
}

func (api *Server) ApiJson() {
	api.Run.GET("/json", func(c *gin.Context) {
		// 获取客户端IP地址
		// 如果有代理服务器，会返回代理服务器的IP
		clientIP := c.ClientIP()
		res := new(Res)
		res.Msg = "你使用了公网访问"
		res.Code = http.StatusOK
		// 检查是否有X-Forwarded-For头
		if forwardedFor := c.GetHeader("X-Forwarded-For"); forwardedFor != "" {
			// 如果有，使用X-Forwarded-For头中的IP地址
			clientIP = forwardedFor
			res.Msg = "你使用了代理/Nat转换请求"
		}
		res.Ip = clientIP

		// 返回JSON响应
		c.JSON(http.StatusOK, res)
	})
}
