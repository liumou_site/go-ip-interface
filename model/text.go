package model

import "fmt"

func (api *Server) GetIndexHtml(ip, bg, url, ver string) string {
	text := fmt.Sprintf(`<!doctype html>
<html lang="zh">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>IP查询</title>
  <link href="/static/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
  <script src="/static/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
</head>
<body style="background-image: url('/static/%s'); background-repeat: no-repeat;background-size: cover;">
<style>
  .container{
    height: 95vh;
    text-align: center;
    padding-top: 46vh;
  }
  .flex-container {
    display: flex;
    justify-content: center;
    align-items: center;
  }
</style>
<div class="container">
  <div class="btn btn-success" style="margin: auto;">
    <p>你的IP: %s</p>
  </div>
    <div class="flex-container">
      <p class="btn btn-outline-light">
        <a href="/json">Json格式</a>
      </p>

      <p class="btn btn-outline-light">
        <a href="/api">文本格式</a>
      </p>
    </div>

    <div class="flex-container">
      <p class="btn btn-outline-light">
        <a href="%s">IPV%s查询</a>
      </p>
      <p class="btn btn-outline-light">
        <a href="https://gitee.com/liumou_site/go-ip-interface">项目主页</a>
      </p>
    </div>

    <div class="flex-container">
      <p class="btn btn-outline-light">
        <a href="http://speed.nuaa.edu.cn">南京航空航天大学测速[4&6]</a>
      </p>
      <p class="btn btn-outline-light">
        <a href="https://test.ustc.edu.cn/">中科大测速[4&6]</a>
      </p>
    </div>

  <div class="btn btn-outline-light" style="margin: auto;">
    <a href="https://liumou.site/">作者博客</a>
  </div>
</div>

</body>

</html>
`, bg, ip, url, ver)
	if api.Debug {
		fmt.Println(text)
	}
	return text
}
