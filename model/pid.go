package model

import (
	"gitee.com/liumou_site/gf"
	"github.com/spf13/cast"
	"os"
)

func (api *Server) Pid() {
	pid := os.Getegid()
	f := gf.NewFile("/run/ips.pid")
	f.Echo(cast.ToString(pid))
}
