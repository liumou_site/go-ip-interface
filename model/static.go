package model

import (
	"gitee.com/liumou_site/gf"
	"gitee.com/liumou_site/logger"
	"path/filepath"
)

func (api *Server) StaticIco() {
	var err error
	f := gf.NewFile(api.Ico)
	f.IsFile()
	if f.IsFiles {
		api.Ico, err = filepath.Abs(api.Ico)
		if err == nil {
			api.Run.StaticFile("/favicon.ico", api.Ico)
			logger.Info("当前设置图标路径: ", api.Ico)
		} else {
			logger.Warn("无法获取绝对路径: ", api.Ico)
		}
	} else {
		logger.Warn("图标文件不存在: ", api.Ico)
	}
}

func (api *Server) StaticHtml() {
	var err error
	f := gf.NewFile(api.Tmpl)
	f.IsDir()
	if f.IsDirs {
		api.Ico, err = filepath.Abs(api.Ico)
		if err == nil {
			api.Run.Static("/static", api.Tmpl)
			logger.Info("当前设置/js路径: ", api.Ico)
		} else {
			logger.Warn("无法获取绝对路径: ", api.Ico)
		}
	} else {
		logger.Warn("图标文件不存在: ", api.Ico)
	}
}
