package model

import (
	"gitee.com/liumou_site/gns"
	"github.com/gin-gonic/gin"
	"net/http"
)

func (api *Server) IndexHtml() {
	api.Run.GET("/", func(c *gin.Context) {
		// 如果有代理服务器，会返回代理服务器的IP
		clientIP := c.ClientIP()

		// 检查是否有X-Forwarded-For头
		if forwardedFor := c.GetHeader("X-Forwarded-For"); forwardedFor != "" {
			// 如果有，使用X-Forwarded-For头中的IP地址
			clientIP = forwardedFor
		}
		// 设置允许的跨域头部
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		// 返回HTML渲染
		c.Header("Content-Type", "text/html; charset=utf-8")
		ip, v := gns.IpParse(clientIP)
		if ip == nil {
			c.String(http.StatusOK, "无法获取IP")
			return
		}
		if v == 4 {
			c.String(http.StatusOK, api.GetIndexHtml(clientIP, api.Bg, api.Urls6, "6"))
		} else {
			c.String(http.StatusOK, api.GetIndexHtml(clientIP, api.Bg, api.Urls4, "4"))
		}
	})

}
