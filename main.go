package main

import (
	"flag"
	"fmt"
	"github.com/gin-gonic/gin"
	"ip/model"
	"os"
)

var (
	port  int    // 启动端口
	ver   bool   // 是否显示版本号
	ico   string // 设置图标
	tmpl  string // 设置样式目录
	bg    string // 背景图标路径
	u6    string // IPV6访问地址
	u4    string // IPV4访问地址
	debug bool   // 显示更多运行信息
	v4    bool   // 监听V4
	v6    bool   // 监听V6
	pid   bool   // 是否写入pid文件
)

func main() {
	v := "1.8.2"
	gfs := flag.NewFlagSet("ips", flag.ExitOnError)
	gfs.StringVar(&ico, "f", "images/favicon.ico", "设置favicon.ico路径")
	gfs.StringVar(&tmpl, "t", "static", "设置static路径")
	gfs.StringVar(&bg, "bg", "bg1.jpg", "设置背景图标,必须放在static目录")
	gfs.StringVar(&u6, "u6", "http://ipv6.liumou.site:22010/", "设置IPV6跳转URL")
	gfs.StringVar(&u4, "u4", "https://ip.liumou.site/", "设置IPV4跳转URL")
	gfs.IntVar(&port, "p", 22010, "设置服务器端口")
	gfs.BoolVar(&v4, "v4", true, "监听IPV4端口-参数已作废")
	gfs.BoolVar(&v6, "v6", false, "监听IPV6端口-参数已作废")
	gfs.BoolVar(&pid, "pid", false, "写入pid文件(用于system管理)")
	gfs.BoolVar(&ver, "v", false, "显示版本号")
	gfs.BoolVar(&debug, "d", false, "显示更多运行信息(debug)")
	args := os.Args
	err := gfs.Parse(args[1:])
	if err != nil {
		fmt.Println("参数异常: ", err.Error())
		os.Exit(2)
	}
	if ver {
		fmt.Println(v)
		os.Exit(0)
	}
	r := gin.Default()
	server := new(model.Server)
	server.Port = port
	server.Run = r
	server.Ico = ico
	server.Tmpl = tmpl
	server.Bg = bg
	server.Urls6 = u6
	server.Urls4 = u4
	server.Debug = debug
	server.Pids = pid
	server.Listen4 = v4
	server.Listen6 = v6
	server.Start()
}
