#!/usr/bin/env bash
f=ips_linux-X86_64
pid=`lsof -i:22010 | awk '{print $2}'| sed -n 2p`
lsof -i:22010
if [[ $? -eq 0 ]];then
  kill -9 $pid
fi