#!/usr/bin/env bash
bash ./kill.sh
go build
if [[ $? -eq 0 ]];then
  echo "构建成功"
  f=ip
  if [[ -e ./$f ]];then
    chmod +x $f
    ./$f &
  else
    echo "文件不存在: $f"
    exit 2
  fi
else
  echo "构建失败"
  if [[ -e ./ips_linux-X86_64 ]];then
    chmod +x ips_linux-X86_64
    ./ips_linux-X86_64 &
  fi
fi