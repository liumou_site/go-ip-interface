#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File    :   restart.py
@Time    :   2023-03-20 18:13
@Author  :   坐公交也用券
@Version :   1.0
@Contact :   faith01238@hotmail.com
@Homepage : https://liumou.site
@Desc    :   重启服务
"""
import os
from os import system, path
from random import randint
from subprocess import getoutput
from time import sleep


class Restart:
	def __init__(self):
		"""
		工作目录
		:param work:
		"""
		self.port = randint(8000, 20000)
		self.arch = getoutput("uname -m")
		if self.arch == "aarch64":
			self.arch = "arm64"
		if self.arch == "x86_64":
			self.arch = "amd64"
		self.work = "/opt/apps/com.liumou.ips/files"
		self.bin = "/usr/local/bin/ips"
		self.stop = path.join(self.work, "stop.py")

	def start(self):
		"""
		开始启动
		:return:
		"""
		system("killall %s" % self.bin)
		system("chmod +x %s" % self.bin)
		os.chdir(self.work)
		cmd = "%s -p %s -t /opt/apps/com.liumou.ips/files/static -f /opt/apps/com.liumou.ips/files/favicon.ico &" % (self.bin, self.port)
		system(cmd)
		sleep(4)
		system("xdg-open http://127.0.0.1:%d" % self.port)


if __name__ == "__main__":
	restart = Restart()
	restart.start()
