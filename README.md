# GoIP接口

## 介绍

使用Go实现的可私有化部署公网IP获取服务，支持IPV4、IPV6(需要服务端支持IPV6)

## 安装教程

[gitee下载页面-Deb及二进制可执行程序](https://gitee.com/liumou_site/go-ip-interface/releases)

[Deb安装包HTTP直链下载页](https://down.liumou.site/client/com.liumou.ips/)


## 使用说明


### 使用帮助信息

```shell
 # ips -h
Usage of ips:
  -bg string
        设置背景图标,必须放在static目录 (default "bg1.jpg")
  -d    显示更多运行信息(debug)
  -f string
        设置favicon.ico路径 (default "images/favicon.ico")
  -p int
        设置服务器端口 (default 22010)
  -pid
        写入pid文件(用于system管理)
  -t string
        设置static路径 (default "static")
  -u4 string
        设置IPV4跳转URL (default "https://ip.liumou.site/")
  -u6 string
        设置IPV6跳转URL (default "http://eq12.debian6.liumou.site:22010/")
  -v    显示版本号
root@l ~/git/gitee/go/go-ip-interface
 # 
```

### Deb安装服务启动

#### 下崽

https://down.liumou.site/client/com.liumou.ips/
#### 安装

```shell
root@ddns:~# dpkg -i com.liumou.ips_linux-amd64_latest.deb 
(Reading database ... 19171 files and directories currently installed.)
Preparing to unpack com.liumou.ips_linux-amd64_latest.deb ...
正在进行安装之前的删除操作...
正在检测程序运行状态...
程序已关闭,可以继续安装
安装之前 [ 安装目录 ] 删除成功: /opt/apps/com.liumou.ips/ 
安装之前 [ 软连接 ] 删除成功: /usr/local/bin/ips 
安装之前 [ /var/lib/dpkg/info/com.liumou.ips.install ] 删除成功: /var/lib/dpkg/info/com.liumou.ips.changelog 
安装之前的删除操作结束!
Unpacking com.liumou.ips (1.8) over (1.8) ...
正在进行卸载清除工作
service删除成功
桌面图标配置删除成功
图标文件删除成功
Setting up com.liumou.ips (1.8) ...
正在执行安装配置 - postinst
正在创建软连接...
ln -s /opt/apps/com.liumou.ips/files/com.liumou.ips_linux /usr/bin/ips
软连接成功: /usr/bin/ips
用户已创建: ips
安装配置执行结束!
installed successfully
Processing triggers for mailcap (3.70+nmu1) ...
root@ddns:~# 
```

安装成功之后，通过下面的命令管理服务

```shell
sudo systemctl start ips.service # 启动服务
```

```shell
sudo systemctl stop ips.service # 停止服务
```

```shell
sudo systemctl restart ips.service # 重启服务
```

```shell
sudo systemctl enable ips.service # 设置服务开机自启
```

```shell
sudo systemctl disable ips.service # 取消服务开机自启
```

```shell
sudo systemctl status ips.service # 查询服务状态
```

## 效果图

### 服务状态

![STATUS](images/STATUS.png)

### 首页-IPV4

![index](images/index.png)

### 首页-IPV6

![index](images/index6.png)

### 局域网

![lan](images/LAN.png)

### Api接口-文本

![Api](images/api.png)

### Api接口-Json

![Api](images/json.png)


### 通过Python请求

![req](images/req.png)

### 启动

![run](images/run.png)


# 访问服务

## 默认端口访问

程序默认以`22010`端口启动,启动成功之后本机访问

http://127.0.0.1:22010

或者将`127.0.0.1`改成自己的IP地址

## 修改端口

如需修改端口，对于`systemd`启动的可使用下面的命令修改,把`80`改成需要使用的端口号即可


```shell
sudo sed -i 's/pid.*/pid -p 80/g' /etc/systemd/system/ips.service
```

对于直接使用二进制的，请根据帮助信息对应传参启动即可



# Demo

[演示站点-IPV4](https://ip.liumou.site/)

[演示站点-IPV6](http://eq12.debian6.liumou.site:22010/)